﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_8_console
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] mas = { { 0, 0, 0 }, { 3, 1, 4 }, { 3, 3, 0 } };
            Console.WriteLine("Kilk ryadiv bez nulevh elementiv = "+NoneZero(mas).ToString());
            MaxMoreOnse(mas);
            SumRyaStop(mas);
            TWOzeroINryad(mas);
        }

        static void TWOzeroINryad(int[,] matr)
        {
            int lic = 0,sum =0;
            int m = matr.GetLength(0);
            int n = matr.GetLength(1);
            for(int m1=0;m1< m;m1++)
            {
                lic = 0;
                for(int n1=0;n1< n;n1++)
                {
                    sum += matr[m1, n1];
                    if (matr[m1, n1] != 0)
                    {
                        lic++;
                    }
                }
                if (lic >= 2) Console.WriteLine(sum.ToString());
            }
        }

        static void SumRyaStop(int[,] matr)
        {
            int m = matr.GetLength(0);
            int n = matr.GetLength(1);
            int sum1 = 0, sum2 = 0;
            if (m == n)
            {
                for(int i=0;i< m;i++)
                {
                    sum1 = 0;
                    sum2 = 0;

                        for (int m1 = 0; m1 < m; m1++)
                        {
                            sum1 += matr[m1,i];
                        }
                    
                    for (int i1 = 0; i1 < m; i1++)
                    {
                        sum2 = 0;
                            for (int n1 = 0; n1 < m; n1++)
                            {
                                sum2 += matr[i1, n1];
                            }
                        if (sum1 == sum2) Console.WriteLine("k"+i.ToString()+" = n"+i1.ToString() +" "+sum1.ToString());
                    }

                }
            }
            else Console.WriteLine("m!=n");
         }

        static int NoneZero(int[,] matr)
        {
            int nonz = 0, lic = 0;
            for (int m = 0; m < matr.GetLength(0); m++)
            {
                nonz = 0;
                for (int n = 0; n < matr.GetLength(1); n++)
                {
                    if (matr[m, n] != 0) nonz++;
                }
                if (nonz == matr.GetLength(1)) lic++;
            }
            return lic;
        }

        static void MaxMoreOnse(int[,] matr)
        {
            int[] mas = new int[matr.GetLength(0) * matr.GetLength(1)];
            int i = 0, max_el = matr[0, 0], min_el = matr[0, 0];
            for (int m = 0; m < matr.GetLength(0); m++)
            {
                for (int n = 0; n < matr.GetLength(1); n++)
                {
                    mas[i] = matr[m, n];
                    i++;
                    if (max_el < matr[m, n]) max_el = matr[m, n];
                    if (min_el > matr[m, n]) min_el = matr[m, n];
                }
            }
            int lic = 0;
            bool os = true;
            while (os)
            {
                lic = 0;
                for (int o = 0; o < mas.Length; o++)
                {
                    if (max_el == mas[o]) lic++;
                    if (lic >= 2)
                    {
                        Console.WriteLine("max el more 2s = " + max_el.ToString());
                        os = false;
                        break;
                    }
                    if (max_el == min_el) {
                        Console.WriteLine("max el more 2s NONE");
                        os = false;
                        break;
                    }
                }
                max_el--;
            }
        }
    }
}
